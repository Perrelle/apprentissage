#pragma once

#include <string>
#include <vector>
#include <iostream>


struct IdxFile
{
  std::vector<unsigned int> dimensions;
  std::vector<unsigned char> data;

  IdxFile(std::istream &is);
  IdxFile(std::string filename);

private:
  void load(std::istream &is);
};
