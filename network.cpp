#include <cmath>
#include <random>
#include <iomanip>

#include <boost/numeric/ublas/operation.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "network.hpp"

namespace ublas = boost::numeric::ublas;


double sigmoid(double z)
{
  return 1.0 / (1.0 + exp(-z));
}

double sigmoid_prime(double z)
{
  return sigmoid(z) * (1.0-sigmoid(z));
}

template<typename F>
void map(Network::vector &source, Network::vector &dest, F f)
{
  std::transform(source.begin(), source.end(), dest.begin(), f);
}

void zero(Network::vector &v)
{
  noalias(v) = ublas::zero_vector<Network::vector::value_type>(v.size());
}

void zero(Network::matrix &m)
{
  noalias(m) =
    ublas::zero_matrix<Network::vector::value_type>(m.size1(), m.size2());
}


Network::Network()
{
  // Nothing to do
}

Network::Network(int input_size, std::vector<int> layer_sizes)
{
  resize(input_size, layer_sizes);
}

void Network::resize(int input_size, std::vector<int> layer_sizes)
{
  layers.clear();

  int prev = input_size;
  for (int size : layer_sizes)
  { 
    layers.emplace_back(Layer{
      matrix(size, prev), matrix(size, prev),
      vector(size), vector(size), vector(size), vector(size), vector(size),
      vector(size), vector(size)
    });

    prev = size;
  }

  randomize();

  // Initialize error data so learning can start immediately
  for (Layer &layer : layers)
  {
    zero(layer.weights_error);
    zero(layer.biases_error);
  }
}

void Network::randomize()
{
  std::mt19937 gen;
  std::normal_distribution<float> dist(0.0f, 1.0f);

  for (Layer &layer : layers)
  {
    for (size_t i = 0 ; i < layer.weights.size1() ; i++)
    {
      for (size_t j = 0; j < layer.weights.size2() ; j++)
        layer.weights(i, j) = dist(gen);
      layer.biases(i) = dist(gen);
    }
  }
}

Network::vector const &Network::feedforward(Network::vector &input)
{
  vector *last = &input;

  for (Layer &layer : layers)
  {
    noalias(layer.z) = ublas::prod(layer.weights, *last) + layer.biases;
    map(layer.z, layer.a, sigmoid);
    last = &layer.a;
  }

  return *last;
}

void Network::backpropagation(Network::vector &input, Network::vector &output)
{
  for (auto layer = layers.rbegin() ; layer != layers.rend() ; ++layer)
  {
    if (layer == layers.rbegin())
    {
      noalias(layer->a_errors) = layer->a - output;
    }
    else
    {
      auto prev = layer - 1;
      noalias(layer->a_errors) = ublas::prod(prev->z_errors, prev->weights);
    }

    map(layer->z, layer->t, sigmoid_prime);
    noalias(layer->z_errors) = ublas::element_prod(layer->a_errors, layer->t);
    vector &next_a = layer + 1 != layers.rend() ? (layer + 1)->a : input;
    noalias(layer->weights_error) += ublas::outer_prod(layer->z_errors, next_a);
    noalias(layer->biases_error) += layer->z_errors;
  }
}

void Network::learn(float eta)
{
  for (Layer &layer : layers)
  {
    noalias(layer.weights) -= eta * layer.weights_error;
    noalias(layer.biases) -= eta * layer.biases_error;
    zero(layer.weights_error);
    zero(layer.biases_error);
  }
}


void Network::output_dot(std::ostream &os)
{
  os << "digraph g {" << std::endl;
  int l = 0;
  for (Layer &layer : layers)
  {
    for (size_t i = 0 ; i < layer.weights.size1() ; i++)
    {
      for (size_t j = 0 ; j < layer.weights.size2() ; j++)
      {
        float w = layer.weights(i,j);
        os << "  "   << (char)('A' + l)     << j
           << " -> " << (char)('A' + l + 1) << i
           << " [label=\"" << w << "\"];" << std::endl;
      }

      float b = layer.biases(i);
      os << "  "   << (char)('A' + l + 1)     << i
         << " [label=\"" << b << "\"];" << std::endl;
    }
    l = l + 1;
  }
  os << "}" << std::endl;
}

void Network::output(std::ostream &os)
{
  int l = 0;
  for (Layer &layer : layers)
  {
    os << std::fixed << std::setprecision(2);
    os << "Layer " << l << std::endl;
    os << "Weights: " << layer.weights << std::endl;
    os << "Biases: " << layer.biases << std::endl;
    os << "z: " << layer.z << std::endl;
    os << "a: " << layer.a << std::endl;
    os << "t: " << layer.t << std::endl;
    os << "weights_error: " << layer.weights_error << std::endl;
    os << "biases_error: " << layer.biases_error << std::endl;
    os << "z_errors: " << layer.z_errors << std::endl;
    os << "a_errors: " << layer.a_errors << std::endl;
    os << std::endl;
    l++;
  }
}

