CPPFLAGS += -I/home/vperrelle/usr/include -DNDEBUG -DBOOST_UBLAS_NDEBUG
CXXFLAGS += -std=c++11 -Wall -pedantic -O3 -g
LDFLAGS += -g
DATA = \
  data/t10k-images-idx3-ubyte \
  data/t10k-labels-idx1-ubyte \
  data/train-images-idx3-ubyte \
  data/train-labels-idx1-ubyte
VPATH += $(LIBRARY_PATH) $(LD_LIBRARY_PATH) # Should contain boost libs


# --- General rules ---

TARGETS=boolean handwritten

.PHONY: all test clean

all: $(TARGETS)

test: test-boolean test-handwritten

clean:
	@$(RM) *.o $(TARGETS)


# --- Testing ---

export TIME=$(shell tput setaf 4)finished in %E$(shell tput sgr0)

.PHONY: test-boolean test-handwritten

test-boolean: boolean
	/usr/bin/time ./boolean \
	  --hidden-neurons 3 \
	  --epoch-count 10 \
	  --epoch-size 10 \
	  --batch-size 10 \
	  --learning-rate 5

test-handwritten: handwritten $(DATA)
	/usr/bin/time ./handwritten \
	  --hidden-neurons 30 \
	  --epoch-count 10 \
	  --epoch-size 100 \
    --batch-size 10 \
	  --evaluation-size 100 \
	  --learning-rate 5.0



# ---  Specific rules ---

boolean: \
	network.o gradient-learning.o \
	test-common.o test-boolean.o -lboost_program_options

handwritten: \
	idx-format.o image.o \
	network.o gradient-learning.o \
	test-common.o test-handwritten.o -lboost_program_options

$(TARGETS):
	$(CXX) $(LDFLAGS) -o $@ $^


# --- Data Retrieval ---

data:
	@mkdir --parent data

$(DATA) : data/%: data/%.gz
	@gzip $< --decompress

$(addsuffix .gz,$(DATA)) : data/%: | data
	@wget --directory-prefix="data" "http://yann.lecun.com/exdb/mnist/$*"

