#include <iostream>
#include <iomanip>
#include <boost/program_options.hpp>

#include "test-common.hpp"

namespace options = boost::program_options;


int run(LearningParameters p, Problem const &problem)
{
  std::cout << std::fixed << std::setprecision(3);
  std::cout << "hidden_neurons: " << p.hidden_neurons << std::endl;
  std::cout << "epoch_count: " << p.epoch_count << std::endl;
  std::cout << "epoch_size: " << p.epoch_size << std::endl;
  std::cout << "batch_size: " << p.batch_size << std::endl;
  std::cout << "evaluation_size: " << p.evaluation_size << std::endl;
  std::cout << "learning_rate: " << p.learning_rate << std::endl;
  std::cout << std::endl;

  Trainer trainer(problem, std::vector<int>{p.hidden_neurons});

  for (int epoch = 1 ; epoch <= p.epoch_count ; epoch ++)
  {
    trainer.train(p.batch_size, p.epoch_size, p.learning_rate);
    Trainer::Evaluation eval = trainer.evaluate(p.evaluation_size);
    std::cout << "Epoch " << epoch << ": "
              << eval.validated << " / " << eval.total << std::endl;
  }

  return 0;
}


int run(LearningParameters p, Problem const &problem, int argc, char *argv[])
{
  options::options_description description("Allowed options");
  description.add_options()
    ("help", "display this help message")
    ("hidden-neurons",
        options::value<int>(&p.hidden_neurons)->default_value(p.hidden_neurons),
        "the number of hidden neuron in the network")
    ("epoch-count",
        options::value<int>(&p.epoch_count)->default_value(p.epoch_count),
        "the number of epochs")
    ("epoch-size",
        options::value<int>(&p.epoch_size)->default_value(p.epoch_size),
        "the size of epochs")
    ("batch-size",
        options::value<int>(&p.batch_size)->default_value(p.batch_size),
        "the size of batches for training")
    ("evaluation-size",
        options::value<int>(&p.evaluation_size)->default_value(p.evaluation_size),
        "the size of batches for evaluation")
    ("learning-rate",
        options::value<float>(&p.learning_rate)->default_value(p.learning_rate),
        "the learning rate of the network")
  ;

  options::variables_map vm;

  try
  {
    options::store(options::parse_command_line(argc, argv, description), vm);
    options::notify(vm);
  }
  catch (options::error &e) 
  {
    std::cerr << e.what() << std::endl;
    std::cerr << description << std::endl;
    return 1;
  }

  if (vm.count("help")) {
    std::cerr << description << std::endl;
    return 1;
  }

  return run(p, problem);
}

