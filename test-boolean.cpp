#include "test-common.hpp"


bool flip()
{
  return std::rand() % 2;
}

float bool_input(bool i)
{
  return i ? 1.0 : 0.0;
}

class BooleanOperator : public GenerativeProblem
{
public:
  BooleanOperator() : GenerativeProblem(2, 2) {}

  data generate() const {
    bool b1 = flip(), b2 = flip();
    std::vector<float> input_data{bool_input(b1), bool_input(b2)};
    std::vector<float> output_data{bool_input(b1 && b2), bool_input(b1 || b2)};
    return data{input_data, output_data};
  }

  bool validate(Network::vector expected, Network::vector given) const {
    return
      (expected[0] > 0.5f) == (given[0] > 0.5f) &&
      (expected[1] > 0.5f) == (given[1] > 0.5f);
  }
};


int main(int argc, char *argv[])
{
  BooleanOperator problem;

  LearningParameters default_parameters;
  default_parameters.hidden_neurons = 3;
  default_parameters.epoch_count = 10;
  default_parameters.epoch_size = 10;
  default_parameters.batch_size = 10;
  default_parameters.evaluation_size = 100;
  default_parameters.learning_rate = 5.0;

  return run(default_parameters, problem, argc, argv);
}

