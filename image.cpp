#include <cassert>

#include "image.hpp"


Image::Image(IdxFile const &file, unsigned int index) :
  width(file.dimensions[2]), height(file.dimensions[1])
{
  assert (index < file.dimensions[0]);
  int size = width * height, start = index * size;
  data.resize(size);
  for (int i = 0 ; i < size ; i++)
    data[i] = file.data[start + i] / 256.0;
}

void Image::draw(std::ostream& os) const
{
  for (unsigned int r = 0, i = 0 ; r < height ; r++)
  {
    for (unsigned int c = 0 ; c < width ; c++, i++)
    {
      float value = data[i];
      char x;
      if (value >= 0.75)
        x = 'O';
      else if (value >= 0.50)
        x = 'o';
      else if (value >= 0.25)
        x = '.';
      else
        x = ' ';
      os << x;
    }
    os << std::endl;
  }
}

