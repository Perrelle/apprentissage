#pragma once

#include "idx-format.hpp"

#include <boost/numeric/ublas/vector.hpp>


struct Image
{
  unsigned int const width, height;
  boost::numeric::ublas::vector<float> data;

  Image(IdxFile const &file, unsigned int index);

  void draw(std::ostream& os = std::cout) const;
};

