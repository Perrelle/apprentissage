#include <vector>
#include <algorithm>
#include <numeric>
#include <random>
#include <chrono>

#include "idx-format.hpp"
#include "image.hpp"
#include "test-common.hpp"


class DigitRecognition : public Problem
{
public:
  DigitRecognition(IdxFile const &_train_images, IdxFile const &train_labels,
    IdxFile const &_eval_images, IdxFile const &eval_labels) :
      Problem(_train_images.dimensions[1] * _train_images.dimensions[2], 10),
      train_labels(train_labels), eval_labels(eval_labels)
  {
    for (unsigned i = 0 ; i < _train_images.dimensions[0] ; i++)
      train_images.emplace_back(_train_images, i);
    for (unsigned i = 0 ; i < _eval_images.dimensions[0] ; i++)
      eval_images.emplace_back(_eval_images, i);
  }

  static Network::vector input_label(IdxFile const& file, unsigned index)
  {
    return boost::numeric::ublas::unit_vector<float>(10, file.data[index]);
  }

  static int output_label(Network::vector &v)
  {
    return std::distance(v.begin(), std::max_element(v.begin(), v.end()));
  }

  epoch training_set(unsigned batch_size, unsigned max_epoch_size) const
  {
    unsigned image_count = train_images.size();
    unsigned seed =
      std::chrono::system_clock::now().time_since_epoch().count();

    std::vector<unsigned> v(image_count);
    std::iota(v.begin(), v.end(), 0);
    std::shuffle(v.begin(), v.end(), std::default_random_engine(seed));

    epoch e;
    for (unsigned i = 0, k = 0 ; i < max_epoch_size && k < image_count ; i++)
    {
      e.emplace_back();
      batch &b = e.back();
      for (unsigned j = 0 ; j < batch_size && k < image_count ; j++, k++)
      {
        unsigned l = v[k];
        b.emplace_back(data{
            train_images[l].data,
            input_label(train_labels, l)
          });
      }
    }
    return e;
  }
  
  batch evaluation_set(unsigned batch_size) const
  {
    batch b;
    for (unsigned j = 0 ; j < batch_size && j < eval_images.size() ; j++)
    {
      b.emplace_back(data{
          eval_images[j].data,
          input_label(eval_labels, j)
        });
    }
    return b;
  }

  bool validate(Network::vector expected, Network::vector given) const {
    return output_label(expected) == output_label(given);
  }

  std::vector<Image> train_images, eval_images;
  IdxFile const &train_labels, &eval_labels;
};


int main(int argc, char *argv[])
{
  try
  {
    std::cout << "Loading images ..." << std::endl;
    IdxFile train_images("data/train-images-idx3-ubyte");
    IdxFile train_labels("data/train-labels-idx1-ubyte");
    IdxFile eval_images("data/t10k-images-idx3-ubyte");
    IdxFile eval_labels("data/t10k-labels-idx1-ubyte");
    std::cout << "Images loaded." << std::endl;
    std::cout << "Training set: " << train_images.dimensions[0] << " images" << std::endl;
    std::cout << "Evaluation set: " << eval_images.dimensions[0] << " images" << std::endl;

    DigitRecognition problem(train_images, train_labels, eval_images, eval_labels);

    LearningParameters default_parameters;
    default_parameters.hidden_neurons = 30;
    default_parameters.epoch_count = 100;
    default_parameters.epoch_size = 10000;
    default_parameters.batch_size = 10;
    default_parameters.evaluation_size = 10000;
    default_parameters.learning_rate = 5.0f;

    return run(default_parameters, problem, argc, argv);
  }
  catch (std::exception &e)
  {
    std::cerr << e.what();
    return 2;
  }
}

