class Perceptron
{
  bool process(bool x1, bool x2, bool x3, int w1, int w2, int w3, int bias) {
    return x1 * w1 + x2 * w2 + x3 * w3 + bias > 0;
  }
};
