#include "gradient-learning.hpp"

// --- Generative problems ---

Problem::epoch GenerativeProblem::training_set(unsigned batch_size, unsigned max_epoch_size) const
{
  epoch e;
  for (unsigned i = 0 ; i < max_epoch_size ; i++)
  {
    e.emplace_back();
    batch &b = e.back();
    for (unsigned j = 0 ; j < batch_size ; j++)
      b.emplace_back(generate());
  }
  return e;
}

Problem::batch GenerativeProblem::evaluation_set(unsigned batch_size) const
{
  batch b;
  for (unsigned j = 0 ; j < batch_size ; j++)
    b.emplace_back(generate());
  return b;
}

double Problem::error(Network::vector expected, Network::vector given) const {
  return boost::numeric::ublas::norm_2(expected - given);
}


// --- Trainers ---

Trainer::Trainer(Problem const &problem, std::vector<int> hidden_layers) :
    net(), problem(problem)
{
  hidden_layers.push_back(problem.output_size);
  net.resize(problem.input_size, hidden_layers);
}

void Trainer::train(unsigned batch_size, unsigned max_epoch_size, float learning_rate)
{
  Problem::epoch epoch = problem.training_set(batch_size, max_epoch_size);

  int b = 0;
  for (Problem::batch &batch : epoch)
  {
    std::cout << "batch " << (b++) << " / " << epoch.size() << "\r";
    std::cout.flush();
    for (Problem::data &data : batch)
    {
      net.feedforward(data.input);
      net.backpropagation(data.input, data.output);
    }
    net.learn(learning_rate / batch.size());
  }
}

Trainer::Evaluation Trainer::evaluate(unsigned batch_size)
{
  std::cout << "validation...\r";
  std::cout.flush();
  Problem::batch batch = problem.evaluation_set(batch_size);

  double error = 0.0f;
  long unsigned int validated = 0;
  for (Problem::data &data : batch)
  {
    Network::vector result = net.feedforward(data.input);
    error += problem.error(data.output, result);
    validated += problem.validate(data.output, result);
  }
  return Evaluation{error, validated, batch.size()};
}

