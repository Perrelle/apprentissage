#pragma once

#include <vector>

#include "network.hpp"

class Problem
{
public:
  struct data {
    Network::vector input, output;
  };

  typedef std::vector<data> batch;
  typedef std::vector<batch> epoch;

  Problem(unsigned input_size, unsigned output_size) :
      input_size(input_size), output_size(output_size) {}
  virtual ~Problem() {}
  virtual epoch training_set(unsigned batch_size, unsigned max_epoch_size) const = 0;
  virtual batch evaluation_set(unsigned batch_size) const = 0;
  virtual double error(Network::vector expected, Network::vector given) const;
  virtual bool validate(Network::vector expected, Network::vector given) const = 0;

  unsigned const input_size;
  unsigned const output_size;
};

class GenerativeProblem : public Problem
{
protected:
  virtual data generate() const = 0;

public:
  GenerativeProblem(unsigned input_size, unsigned output_size)
      : Problem(input_size, output_size) {}
  epoch training_set(unsigned batch_size, unsigned max_epoch_size) const;
  batch evaluation_set(unsigned batch_size) const;
};

class Trainer
{
public:
  struct Evaluation {
    double error;
    long unsigned int validated, total;
  };

  Trainer(Problem const &problem, std::vector<int> hidden_layers);

  void train(unsigned batch_size, unsigned max_epoch_size, float learning_rate);
  Evaluation evaluate(unsigned batch_size);

  Network net;
  Problem const& problem;
};

