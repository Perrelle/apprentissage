#pragma once

#include "gradient-learning.hpp"


struct LearningParameters
{
  int hidden_neurons;
  int epoch_count, epoch_size, batch_size, evaluation_size;
  float learning_rate;
};

int run(LearningParameters p, Problem const &problem);
int run(LearningParameters default_parameters, Problem const &problem,
    int argc, char *argv[]);

