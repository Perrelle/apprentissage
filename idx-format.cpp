#include <cstring>

#include <stdexcept>
#include <fstream>

#include "idx-format.hpp"


// --- std::istream primitives ---

template<typename T>
T read(std::istream &is);

template<>
unsigned int read<unsigned int>(std::istream &is) {
  unsigned char buffer[4];
  is.read(reinterpret_cast<char *>(buffer), 4);
  return buffer[0] << 24 | buffer[1] << 16 | buffer[2] << 8 | buffer[3];
}

template<>
unsigned char read<unsigned char>(std::istream &is) {
  unsigned char c;
  is.read(reinterpret_cast<char *>(&c), 1);
  return c;
}


// --- Idx Files ---

IdxFile::IdxFile(std::istream &is)
{
  load(is);
}

IdxFile::IdxFile(std::string filename)
{
  try
  {
    std::ifstream file(filename);
    file.exceptions(std::ifstream::badbit);
    if (file.fail())
      throw std::ifstream::failure("cannot open file");
    load(file);
  }
  catch (std::ifstream::failure e)
  {
    throw std::ifstream::failure(filename + ": " + strerror(errno));
  }
}

void IdxFile::load(std::istream &is)
{
  int dimension_count, size = 1;
  unsigned int magic_number = read<unsigned int>(is);
  switch (magic_number)
  {
    case 2049:
      dimension_count = 1;
      break;

    case 2051:
      dimension_count = 3;
      break;

    default:
      throw new std::runtime_error("Invalid format");
  }

  for (int d = 0 ; d < dimension_count ; d++)
  {
    int v = read<unsigned int>(is);
    dimensions.push_back(v);
    size *= v;
  }

  for (int i = 0 ; i < size ; i++)
  {
    data.push_back(read<unsigned char>(is));
  }
}

