#pragma once

#include <vector>
#include <iostream>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>


class Network {
public:
  typedef boost::numeric::ublas::vector<float, std::vector<float>> vector;
  typedef boost::numeric::ublas::matrix<float> matrix;

  Network();
  Network(int input_size, std::vector<int> layer_sizes);
  void resize(int input_size, std::vector<int> layer_sizes);
  void randomize();

  vector const &feedforward(vector &input);
  void backpropagation(Network::vector &input, Network::vector &output);
  void learn(float eta);

  void output_dot(std::ostream &os);
  void output(std::ostream &os);

private:
  struct Layer {
    // Invariant: matrix row count equal vectors size and max and matrix
    // column count equal previous layer matrix row count or input size
    matrix weights, weights_error;
    vector biases, biases_error, z, z_errors, a, a_errors, t;
  };

  std::vector<Layer> layers;
};

